package com.example.restservice;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import com.google.cloud.spring.data.datastore.core.mapping.Unindexed;

import org.springframework.data.annotation.Id;


@Entity(name = "Forum")
public class Forum {
	@Id
	private Long id;
	
	private String title;
	
	private String date;
	
	@Unindexed
	private String content;
	
	private int likes;
	
	private String type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Forum [id=" + id + ", title=" + title + ", date=" + date + ", content=" + content + ", likes=" + likes
				+ ", type=" + type + "]";
	}
	
	
}
