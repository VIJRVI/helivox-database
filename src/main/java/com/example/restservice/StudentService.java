package com.example.restservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.EntityQuery;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.KeyFactory;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.QueryResults;

public class StudentService {
	

	
	public void saveStudent(Student student) {
		
		 Datastore datastore = DatastoreOptions.getDefaultInstance().getService();
		 KeyFactory keyFactory =  datastore.newKeyFactory();
		
		 Key key = datastore.allocateId(keyFactory.setKind("student").newKey());
		 
	        
	        Entity.Builder builder = Entity.newBuilder(key)
	                .set("hours", (student.getHours()));

	        Entity studentEntity = builder.build();
	        datastore.put(studentEntity);

	}
	
	public List<Long> getAllStudentsHours() {
		
		 Datastore datastore = DatastoreOptions.getDefaultInstance().getService();
		
		 EntityQuery.Builder queryBuilder = Query.newEntityQueryBuilder().setKind("student");
		 EntityQuery query = queryBuilder.build();
	     QueryResults<Entity> results = datastore.run(query);
	     ArrayList<Long> hours = new ArrayList<Long>();
	     while (results.hasNext()) {
	    	 Entity student = results.next();
	    	 hours.add(student.getLong("hours"));
	    	 
	     }
	     return hours;

	}

}
