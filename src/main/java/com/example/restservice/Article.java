package com.example.restservice;

import org.springframework.data.annotation.Id;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import com.google.cloud.spring.data.datastore.core.mapping.Unindexed;

@Entity(name = "Article")
public class Article {
	@Id
	private Long id;
	
	@Unindexed
	private String content;
	
	private String category;
	
	private String author;
	
	private String color;
	
	private String title;
	
	private String date;
	
	@Unindexed
	private String titleImage;
	

	public String getTitle() {
		return title;
	}
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTitleImage() {
		return titleImage;
	}

	public void setTitleImage(String titleImage) {
		this.titleImage = titleImage;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", content=" + content + ", category=" + category + ", author=" + author
				+ ", color=" + color + ", title=" + title + ", date=" + date + ", titleImage=" + titleImage + "]";
	}


	
	
	
}
