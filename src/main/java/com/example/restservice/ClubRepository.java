package com.example.restservice;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

import java.util.List;


public interface ClubRepository extends DatastoreRepository<Clubs, Long> {
	
	List<Clubs> findByTitle(String title);
	
	List<Clubs> findByTags(List<String> tags);
		
}
