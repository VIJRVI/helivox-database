package com.example.restservice;

import java.util.List;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;



public interface ArticleRepository extends DatastoreRepository<Article, Long> {

	List<Article> findByTitle(String title);

		
}