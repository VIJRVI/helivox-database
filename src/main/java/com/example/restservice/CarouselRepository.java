package com.example.restservice;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;



public interface CarouselRepository extends DatastoreRepository<Carousel, Long> {

}