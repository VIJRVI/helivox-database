package com.example.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {
	
	@Autowired
    ClubRepository clubrepo;
	
	@Autowired
    CourseRepository courseRepo;
	
	@Autowired
    AdminRepository adminRepo;
	
	@Autowired
    UserRepository userRepo;
	
	@Autowired
    ForumRepository forumRepo;
	
	@Autowired
	ArticleRepository articleRepo;
	
	@Autowired
	CarouselRepository carRepo;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
