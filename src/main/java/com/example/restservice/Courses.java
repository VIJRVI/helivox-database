package com.example.restservice;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import com.google.cloud.spring.data.datastore.core.mapping.Unindexed;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

@Entity(name = "Courses")

public class Courses {
	
	@Id
	private Long id;
	
	private String title;
	
	private String category;
	
	private String school;
	
	@Unindexed
	private String description;
	
	@Unindexed
	private String image;
	
	private List<String> tags = new ArrayList<String>();
	
	private List<String> comments = new ArrayList<String>();
	
	private List<String> commentsUnapproved = new ArrayList<String>();
	
	private String link;
	
	private int hrsCommit;
	
	private int cost;

	
	
	
	
	@Override
	public String toString() {
		return "Courses [id=" + id + ", title=" + title + ", category=" + category + ", school=" + school
				+ ", description=" + description + ", image=" + image + ", tags=" + tags + ", comments=" + comments
				+ ", commentsUnapproved=" + commentsUnapproved + ", link=" + link + ", hrsCommit=" + hrsCommit
				+ ", cost=" + cost + "]";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<String> getComments() {
		return comments;
	}

	public void setComments(List<String> comments) {
		this.comments = comments;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public int getHrsCommit() {
		return hrsCommit;
	}

	public void setHrsCommit(int hrsCommit) {
		this.hrsCommit = hrsCommit;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
	
	public List<String> getCommentsUnapproved() {
		return commentsUnapproved;
	}

	public void setCommentsUnapproved(List<String> commentsUnapproved) {
		this.commentsUnapproved = commentsUnapproved;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	
}
