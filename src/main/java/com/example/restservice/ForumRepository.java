package com.example.restservice;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;



public interface ForumRepository extends DatastoreRepository<Forum, Long> {

		
}