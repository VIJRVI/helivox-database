package com.example.restservice;

import org.springframework.data.annotation.Id;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import com.google.cloud.spring.data.datastore.core.mapping.Unindexed;

@Entity(name = "Carousel")
public class Carousel {
	@Id
	private Long id;
	
	@Unindexed
	private String img;
	

	private String originURL;
	
	private String signupURL;
	
	private String opImg;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}



	public String getOriginURL() {
		return originURL;
	}

	public void setOriginURL(String originURL) {
		this.originURL = originURL;
	}

	public String getSignupURL() {
		return signupURL;
	}

	public void setSignupURL(String signupURL) {
		this.signupURL = signupURL;
	}

	public String getOpImg() {
		return opImg;
	}

	public void setOpImg(String opImg) {
		this.opImg = opImg;
	}

	@Override
	public String toString() {
		return "Carousel [id=" + id + ", img=" + img  + ", originURL=" + originURL
				+ ", signupURL=" + signupURL + ", opImg=" + opImg + "]";
	}
	

	
	

}
