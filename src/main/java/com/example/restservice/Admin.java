package com.example.restservice;

import org.springframework.data.annotation.Id;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;

@Entity(name = "Admin")
public class Admin {
	@Id
	private String id;
	
	private String name;
	
	private String type;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	@Override
	public String toString() {
		return "Admin [id=" + id + ", name=" + name + ", type=" + type + ", password=" + "]";
	}
	
	
	

}
