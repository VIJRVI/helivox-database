package com.example.restservice;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

import java.util.List;


public interface CourseRepository extends DatastoreRepository<Courses, Long> {
	
	List<Courses> findByTitle(String title);
	
	List<Courses> findByTags(List<String> tags);

		
}
