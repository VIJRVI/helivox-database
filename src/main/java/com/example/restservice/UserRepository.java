package com.example.restservice;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;



public interface UserRepository extends DatastoreRepository<User, Long> {
	boolean existsByPassword(String password);
	boolean existsByName(String name);



		
}
