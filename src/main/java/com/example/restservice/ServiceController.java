package com.example.restservice;

import java.util.*;

import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin (origins = "*")
public class ServiceController {

	StudentService service = new StudentService();
	
	ClubRepository clubRepo;

	CourseRepository courseRepo;
	
	AdminRepository adminRepo;
	
	UserRepository userRepo;
	
	ForumRepository forumRepo;
	
	ArticleRepository articleRepo;
	
	CarouselRepository carRepo;
	
	
	
	
	@GetMapping("/savedta")
	public void saveDta(@RequestParam(value = "hours", defaultValue = "0") String hours) {
		service.saveStudent(new Student(Long.parseLong(hours)));
	}
	
	@GetMapping("/getall")
	public List<Long> getAll()  {
		return service.getAllStudentsHours();
	}
	
	
	
	
	
	@GetMapping("/getclubs")
    Iterable<Clubs> getClubs() {
        return clubRepo.findAll();
    }

	
	 @PostMapping("/saveclub")
	 Clubs saveClub(@RequestBody Clubs c){
	    this.clubRepo.save(c);
	    return c;
	 }
	 
	 @GetMapping("/getcourse")
	 Iterable<Courses> getCourse() {
	    return courseRepo.findAll();
	 }
	 
	
		
	 @PostMapping("/savecourse")
	 Courses saveCourse(@RequestBody Courses c){
		this.courseRepo.save(c);
		return c;
	 }
	
	 @GetMapping("/find-title/{title}")
	 Iterable<Clubs> getClubsByTitle(@PathVariable("title") String title) {
	    return clubRepo.findByTitle(title);
	 }
	 @GetMapping("/find-article/{article}")
	 Iterable<Article> getArticleByTitle(@PathVariable("article") String article) {
	    return articleRepo.findByTitle(article);
	 }
	 
	 @GetMapping("/find-course-title/{title}")
	 Iterable<Courses> getCourseByTitle(@PathVariable("title") String title) {
	    return courseRepo.findByTitle(title);
	 }
	 
	 @GetMapping("/isAdmin/{password}")
	 Optional<Admin> isAdmin(@PathVariable("password") String password) {
	    return adminRepo.findById(password);
	 }
	 
	 @GetMapping("/findCarousel/{password}")
	 Optional<Carousel> findCarousel(@PathVariable("password") long password) {
	    return carRepo.findById(password);
	 }
	 
	 @GetMapping("/findArticle/{password}")
	 Optional<Article> findArticle(@PathVariable("password") long password) {
	    return articleRepo.findById(password);
	 }
	 
	 @GetMapping("/isUser/{name}/{password}")
	 boolean isUser(@PathVariable("password") String password,@PathVariable("name") String name ) {
		 return (userRepo.existsByPassword(password) && userRepo.existsByName(name));
	 }
	 
	 @PostMapping("/saveUser")
	 User saveUser(@RequestBody User c){
		this.userRepo.save(c);
		return c;
	 }
	 
	 @GetMapping("/getforum")
	    Iterable<Forum> getForum() {
	        return forumRepo.findAll();
	 }
	 
	 @PostMapping("/saveforum")
	 Forum saveForum(@RequestBody Forum c){
		this.forumRepo.save(c);
		return c;
	 }
	 @GetMapping("/getCarousel")
	    Iterable<Carousel> getCarousel() {
	        return carRepo.findAll();
	 }
	 
	 @PostMapping("/deleteCarousel")
	 void deleteCarousel(@RequestBody Carousel c){
		this.carRepo.delete(c);
	 }

	 
	 @PostMapping("/saveCarousel")
	 Carousel saveCarousel(@RequestBody Carousel c){
		this.carRepo.save(c);
		return c;
	 }
	 
	 @PostMapping("/savearticle")
	 Article saveArticle(@RequestBody Article c){
		this.articleRepo.save(c);
		return c;
	 }
	 
	 @GetMapping("/getarticles")
     Iterable<Article> getArticle() {
        return articleRepo.findAll();
     }
	 
	 @PostMapping("/deletearticle")
	 void deleteArticle(@RequestBody Article c){
		this.articleRepo.delete(c);
	 }


	 @PostMapping("/deleteforum")
	 void deleteForum(@RequestBody Forum c){
		this.forumRepo.delete(c);
	 }
	 
	 @PostMapping("/deletecourse")
	 void deleteCourse(@RequestBody Courses c){
		this.courseRepo.delete(c);
	 }
	 
	 @PostMapping("/deleteclub")
	 void deleteClub(@RequestBody Clubs c){
		this.clubRepo.delete(c);
	 }
	 
	 
	
	 


	 ServiceController(ClubRepository s, CourseRepository e, AdminRepository a, UserRepository u, ForumRepository f, ArticleRepository art, CarouselRepository car) {
	        this.clubRepo = s;
	        this.courseRepo = e;
	        this.adminRepo = a;
	        this.userRepo = u;
	        this.forumRepo = f;
	        this.articleRepo = art;
	        this.carRepo = car;
	 }
	 
	 
	 
	 
	 
	
}
